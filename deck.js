// deck.js - main slide entrypoint
// Files need to be explicily imported...

import s00 from './slides/00-intro.mdx'
import s10 from './slides/10-setup.mdx'
import s20 from './slides/20-basics.mdx'
import s30 from './slides/30-observing.mdx'
import s40 from './slides/40-yaml.mdx'
import s50 from './slides/50-skaffold.mdx'
import s98 from './slides/98-planned.mdx'
import s99 from './slides/99-references.mdx'


export default [
  ...s00,
  ...s10,
  ...s20,
  ...s30,
  ...s40,
  ...s50,
  ...s98,
  ...s99,
]

export { default as theme } from 'theme.js'

export { components } from "mdx-deck-code-surfer"
