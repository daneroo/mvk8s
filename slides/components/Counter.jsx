import React, { useState, useEffect, Fragment } from 'react';
import useInterval from './useInterval';

export function Counter() {
  const [count, setCount] = useState(0)

  useInterval(() => {
    // Your custom logic here
    setCount(count + 1);
  }, 1000)

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}


export function Fetch({url='http://localhost:8000/api/v1/hashnow'}) {
  const [data, setData] = useState('...');
  const [query, setQuery] = useState(url)

  const fetchData = () => {
    fetch(query+`?nonce=${+new Date()}`)
      .then(resp => {
        // console.log({resp})
        if (resp.ok) {
          return resp.json()
        } else {
          return Promise.reject({ status: resp.status });
        }
      })
      .then(json => {
        // console.log({json,z:typeof json})
        setData(JSON.stringify(json,null,2))
      })
      .catch(error=>{
        console.error(error)
        setData(error.message)
      })
  }

  // useEffect(() => {
  //   fetchData();
  // }, []);
  useInterval(() => {
    // setCount(count + 1);
    fetchData();
  }, 500)


  return (
    <div>
      <input
        type="text"
        value={query}
        onChange={event => setQuery(event.target.value)}
      />
      <pre style={{textAlign:'left', fontSize:'70%'}}>
        {data}
      </pre>
    </div>
  );
}


export  function Poll() {
  const [polling, setPolling] = useState(true)
  const toggle = () => setPolling(!polling)
  return (
    <div>
      <button onClick={toggle}>
        {polling ? 'Stop Polling' : 'Start Polling'}
      </button>
      { polling && <Fetch />}
    </div>
  )
}

