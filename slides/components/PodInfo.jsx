import React, { Fragment, useState } from 'react';
import usePolling from './usePolling';

const url = 'http://localhost:8080'
export default function PodInfo() {
  const [body, setBody] = useState('ZZZ')
  const [isPolling, startPolling, stopPolling] = usePolling({
    url,
    interval: 1000, // in milliseconds(ms)
    retryCount: 3, // this is optional
    onSuccess: (data) => {
      console.log('got',data)
      if ((typeof data) === 'object'){
        data = JSON.stringify(data,null,2)
      }
      setBody(data)
      return true
    },
    onFailure: (error) => {
      setBody(error.message)
      console.error(error)
    }, // this is optional
    // method: 'GET',
    initialyPolling:false
  });

  return (
    <div>
      {isPolling ? (
        <Fragment>
          <pre style={{textAlign:'left', fontSize:'70%'}}>{body}</pre>
          <button onClick={stopPolling}>Stop Polling</button>
        </Fragment>
      ) : (
        <Fragment>
          <div> ... </div>
          <button onClick={startPolling}>Start Polling</button>
        </Fragment>
      )}
    </div>
  );
}

