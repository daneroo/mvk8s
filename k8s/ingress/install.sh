#!/usr/bin/env bash

# Nginx ingress controller:
#  Docs: https://kubernetes.github.io/ingress-nginx/deploy/#prerequisite-generic-deployment-command

# Namespace, roles and config maps
MANDATORY_MANIFESTS="https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml"
kubectl apply -f ${MANDATORY_MANIFESTS}

# Docker for desktop Service (LoadBalancer)
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml

# if using BareMetal, would use NodePort:
# kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/baremetal/service-nodeport.yaml