#!/usr/bin/env bash

echo
echo "This is your token to login at:"
echo "  https://localhost:8443"
echo
kubectl  get \
 $(kubectl get secret -o name -n kube-system |grep replicaset) \
 -n kube-system -o json | jq -r .data.token | base64 -D
 echo

(sleep 3; open https://localhost:8443) &

echo
echo "Only exit when you no longer want access to the dashboard"
echo "  exit with Crtl-C"
echo
kubectl port-forward \
  $(kubectl get pod -o name -n kube-system |grep dashboard) \
  8443:8443 --namespace=kube-system
# ctrl-C to exit, but not yet

echo
echo You can now uninstall the dash board if you wish
echo $(dirname $0)/uninstall.sh
