#!/usr/bin/env bash

# DASHBOARD_MANIFESTS="https://goo.gl/2TkwJk "
DASHBOARD_MANIFESTS="https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml"

kubectl create -f ${DASHBOARD_MANIFESTS}

echo
echo Now run the port forwarding proxy
echo $(dirname $0)/port-forward.sh
