
#!/usr/bin/env bash

# DASHBOARD_MANIFESTS="https://goo.gl/2TkwJk "
DASHBOARD_MANIFESTS="https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml"

kubectl delete -f ${DASHBOARD_MANIFESTS}
