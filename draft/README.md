# Draft experiment

- ingress using hostname:

## Operating

```bash
draft up
draft delete
```

## Load

```bash
while true; do
  hey -n 200 -c 10 -q 2 http://localhost:8080/api/v1/hashnow; sleep 1;
done
```

## Setup

```bash
brew install azure/draft/draft
draft init
draft config set registry docker.io/daneroo
draft create -a mvk8s
helm init
```
