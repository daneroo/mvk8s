const pkg = require('../package.json')
module.exports = {
  name: pkg.name,
  version: pkg.version,
  hostname: require('os').hostname(),
  express: {
    port: process.env.PORT || 8000
  }
}
