
const { delay } = require('./delay')
const log = console

module.exports = {
  setup
}

// docker stop first sends a SIGTERM, after 10 seconds a SIGKILL
//  but you can also send a SIGINT: docker kill --signal SIGINT compose_tau-taurus_1
// Issuing a Ctrl-C from the shell to a running `npm start` process sends a SIGINT also,
//  but it seems to be handleed twice

let needsSetupLatch = true
function setup (onShutdown) {
  if (needsSetupLatch) {
    needsSetupLatch = false

    process.on('SIGTERM', shutDown) // sent by docker-compose down, docker stop
    process.on('SIGINT', shutDown)
  }
  async function shutDown (signal) {
    log.info(`${new Date().toISOString()}  Shutting down after receiving ${signal}`)

    if (onShutdown) {
      await onShutdown() // this is not as clean as it could be
    }
    await delay(1000)
    log.warn(`${new Date().toISOString()}  This could have been be more graceful, but this is fast`)
    process.exit(0)
  }
}
