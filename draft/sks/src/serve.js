'use strict'

module.exports = {
  start,
  stop,
  newServer,
  defaultRouter,
  slashHandler,
  versionHandler,
  healthHandler
}

// dependencies - core-public-internal
const crypto = require('crypto')
const http = require('http')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const log = console
const config = require('./config')

// User global config singleton
const singleton = newServer()
singleton.app.use(function (req, res, next) {
  log.info(`${new Date().toISOString()}  ${req.path}`)
  next()
})

singleton.app.use(defaultRouter())
singleton.app.use('/api/v1', apiRouter())
// express init -
// TODO(daneroo): through singleton?
function newServer (/* config */) {
  const app = express()
  const server = http.createServer(app)
  return { app, server }
}

// should be a method of (newServer), but don;t want any Objects or classes...
async function start () {
  // get the singleton, so we can stop()...
  const { server } = singleton

  return new Promise((resolve, reject) => {
    server.listen(config.express.port, function (error) {
      if (error) {
        log.error(error)
        reject(error)
      }

      log.info(`${new Date().toISOString()} ${config.name} server:${config.version} server @${config.hostname} listening on port *:${config.express.port}`)
      resolve()
    })
  })
}

function stop () {
  const { server } = singleton
  server.close()
}

// Extract, this is just an example router
function defaultRouter (/* config */) {
  const router = express.Router()

  router.use(cors())

  // define the home page route
  router.get('/', slashHandler)
  router.get('/version', versionHandler)
  router.get('/health', healthHandler)

  return router
}

function slashHandler (req, res) {
  res.json({
    hostname: config.hostname,
    stamp: new Date().toISOString(),
    name: config.name,
    version: config.version,
    status: 'OK'
  })
}

function versionHandler (req, res) {
  res.json({
    hostname: config.hostname,
    stamp: new Date().toISOString(),
    version: config.version
  })
}

function healthHandler (req, res) {
  const stamp = new Date().toISOString()
  res.json({
    hostname: config.hostname,
    stamp,
    status: 'OK'
  })
}

// mount at /api/v1
function apiRouter (/* config */) {
  const router = express.Router()

  router.use(cors())

  // Crawl related routes
  router.use(bodyParser.json())
  router.get('/hashnow', hashNowHandler)
  return router
}

function hashNowHandler (req, res) {
  const stamp = new Date().toISOString()
  const hash = crypto.createHash('sha1').update(stamp).digest('hex')
  const payload = {
    hostname: config.hostname,
    stamp,
    hash
  }
  res.json(payload)
}
