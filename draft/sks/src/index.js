
const { start, stop } = require('./serve')
const shutdown = require('./shutdown')
const log = console

shutdown.setup(async () => {
  log.info(`${new Date().toISOString()}  Server is shutting down`)
  await stop()
  // pre-empt the 1 second delay
  process.exit()
})

main()
async function main () {
  serve()
}

async function serve () {
  try {
    await start()
  } catch (error) {
    log.error('serve::error', error)
    process.exit()
  }
}
