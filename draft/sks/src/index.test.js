'use strict'
// const { isError, preSeraialize } = require('..')

describe('hello', () => {
  describe('basic', () => {
    test.each([
      // [name, expected, v],
      [ 'false', false, false ],
      [ 'true', true, true ]
    ])('Check identity(%s) is %s', (name, expected, v) => {
      expect(v).toEqual(expected)
    })
  })
})
