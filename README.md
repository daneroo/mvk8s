# MVK8S - Minimum Viable Kubernetes

## TODO

- <https://github.com/luxas/kubeadm-workshop>
- Ingress hints: <https://github.com/cds-snc/elenchos>
- Demo with <https://serveo.net/>
  - `ssh -R 80:localhost:8000 serveo.net`
- Ngrok lite: <https://dev.to/k4ml/poor-man-ngrok-with-tcp-proxy-and-ssh-reverse-tunnel-1fm>
- Link to [Katacoda](https://www.katacoda.com/learn)
- App: replace podinfo with `sks`
  - draft + watch
  - skaffold

- lerna with slides, and app/sks
- [Graphs?](https://www.hanselman.com/blog/HowToSetUpKubernetesOnWindows10WithDockerForWindowsAndRunASPNETCore.aspx)
- make two separate themes (mdx-deck + CodeSurfer)
  - nightOwl and solarizedDark
- add (Making of) slide...

- cloning instructions (where's the code)
- pip3 install termtosvg
- CI/CD on gitlab.com (pages, zeit/now and netlify)
  - [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/quick_start_guide.html)
- Workers
  - [node-resque](https://github.com/taskrabbit/node-resque)
  - [goworker](https://github.com/benmanns/goworker)
  - [go-resque](https://github.com/kavu/go-resque)

## Operating

### Starting the server for development

```bash
npm install

npm start
# or equivalently
npx mdx-deck deck.js
```

### Publishing to local directory (`public/`)

```bash
npm run build
open public/index.html
```

### Publishing to (zeit/now)

```bash
npm run build && now --public -n mvk8s public
now rm mvk8s-vsftodkxhi.now.sh
```

### Publishing to netlify

```bash
npm run build && netlifyctl deploy -b public -n mvk8s
# delete with netlify web app....
```

## Checking out this repo

```bash
git clone git@gitlab.com:daneroo/mvk8s.git
```

## Initial setup for slide deck

```bash
npm i -D mdx-deck
npx mdx-deck deck.mdx
```

### Multiple files

Make a `deck.js`, import the other (`.mdx`) files.
See [multiple files docs](https://github.com/jxnblk/mdx-deck/blob/master/docs/advanced.md#combining-multiple-mdx-files)

### Theming

Make a `theme.js` file and import it from `deck.js`

We are using two theming schemes:

- `mdx-deck`'s
- CodeSurfers' which uses [prism-react-renderer](https://github.com/FormidableLabs/prism-react-renderer/tree/master/themes)

We like these themes from `mdx-deck`: `default`, `dark`, `code`,`condensed` and `future`.

For Color schemes, we like CodeSurfer/prism-react's NighOwl, and from a previous version of this site (hugo/reveal) we like `moon.scss`/SolarizedDark

### Font Awesome

This is how we get  [FontAwesome Icons](https://github.com/FortAwesome/react-fontawesome)

```bash
npm i -D \
  @fortawesome/fontawesome-svg-core \
  @fortawesome/free-solid-svg-icons \
  @fortawesome/react-fontawesome \
  @fortawesome/free-brands-svg-icons
```

and use with:

```js
// at top of the mdx:
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'

//... in the slide content:

<span>
  @daneroo :
  <FontAwesomeIcon icon={faTwitter} />
</span>

```

### CodeSurfer

Use the [CodeSurfer](https://github.com/pomber/code-surfer)
component for presenting code snippets

```bash
npm i -D mdx-deck-code-surfer raw-loader
```

default theme in `theme.js`, can be changed with themes from [prism-react-renderer](https://github.com/FormidableLabs/prism-react-renderer/tree/master/themes)

and use with:

```js
// at top of the mdx:
import { CodeSurfer } from "mdx-deck-code-surfer"

<CodeSurfer
  title="Some Title"
  code={require("!raw-loader!./my-snippet.js")}
  lang="javascript"
  showNumbers={false}
  dark={false}
  steps={[
    { notes: "Start with this note"},
    { lines: [6], notes: "Note for the first step" },
    { range: [5, 9] },
    { tokens: { 9: [3, 4, 5] }, notes: "Note for the third step" }
  ]}
/>
```

But if you want to use Code Blocks instead of JSX (`<CodeSurfer />`), then you must do so globally, which means that regular code blocks no longer work (is missing Language name, and:

```js
export { components } from "mdx-deck-code-surfer"
```

This interacts with the multiple files approach above.

You can also set a default prism theme for all CodeSurefer blocks in theme.js:

```js
{
  ...
  codeSurfer: {
  ...nightOwl,
  showNumbers: false
  }
  ...
}
```